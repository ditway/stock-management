package com.company;

interface Callback {
    void execute(Task task);
}
